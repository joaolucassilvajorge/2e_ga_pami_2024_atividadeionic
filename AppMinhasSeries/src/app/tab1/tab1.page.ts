import { Component } from '@angular/core';
import { Series } from '../series.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  seriesList: Series[] = [
    {
      title: 'Black Mirror',
      subtitle: '2011 ‧ Ficção científica ‧ 6 temporadas',
      image: 'https://th.bing.com/th/id/OIP.b_O98jeuwiqYCpBxnlRk7AHaD4?rs=1&pid=ImgDetMain',
      description: '"Black Mirror" é uma série de antologia que explora as consequências perturbadoras e muitas vezes distópicas do avanço tecnológico na sociedade contemporânea. Criada por Charlie Brooker, cada episódio apresenta uma história independente que mergulha em temas como o uso excessivo de redes sociais, inteligência artificial, realidade virtual, vigilância e privacidade. A série ganhou destaque por sua abordagem provocativa e por refletir sobre as complexidades morais e éticas associadas ao progresso tecnológico. Cada episódio funciona como uma crítica satírica e sombria, levantando questões profundas sobre o impacto das tecnologias emergentes no comportamento humano e na estrutura social.',
      isExpanded: false,
      isFavorited: false
    },
    {
      title: 'Breaking Bad',
      subtitle: '2008 ‧ Drama ‧ 5 temporadas',
      image: 'https://goinswriter.com/wp-content/uploads/2013/10/breaking-bad.png',
      description: '"Breaking Bad" é uma série de drama que acompanha a transformação de Walter White, um professor de química que se torna fabricante de metanfetamina após ser diagnosticado com câncer terminal. Com a ajuda de seu ex-aluno, Jesse Pinkman, Walter mergulha no mundo do crime para garantir o futuro financeiro de sua família. Ao longo das temporadas, Walter enfrenta dilemas éticos e morais enquanto se envolve cada vez mais com o tráfico de drogas, lutando para manter seu segredo e proteger aqueles ao seu redor das consequências de suas escolhas. A série é conhecida por sua narrativa intensa, personagens complexos e uma exploração profunda da linha tênue entre o bem e o mal.',
      isExpanded: false,
      isFavorited: false
    },
    {
      title: 'Love, Death & Robots',
      subtitle: '2019 ‧ Terror ‧ 3 temporadas',
      image: 'https://dnm.nflximg.net/api/v6/BvVbc2Wxr2w6QuoANoSpJKEIWjQ/AAAAQTS0K4rlNc_RO_LQ5Q3ruTYZRhEBFQ3WV5UAGaqepT5OfUE9Q6XKMo-uzuG8eZeJbbRrLNKntQsRdzRvo0NZ5FrnYWNAkWTtd4aTyPQwkJOAfE63kJBEa6KnjrD3DGqx8uW0NSld_xTC_d06Lp2mlb9YwGA.jpg?r=92e',
      description: '"Love, Death & Robots" é uma série antológica da Netflix que combina elementos de ficção científica, fantasia e horror em histórias curtas animadas. Cada episódio apresenta uma narrativa única e independente, explorando temas diversos como inteligência artificial, viagem no tempo, e dilemas éticos. A série se destaca pela diversidade visual e temática, abrangendo desde o cômico até o sombrio, sempre com uma abordagem adulta e provocativa. Com sua mistura de estilos visuais e enredos inovadores, "Love, Death & Robots" atrai tanto fãs de animação quanto de ficção especulativa, oferecendo uma experiência intensa e multifacetada a seus espectadores.',
      isExpanded: false,
      isFavorited: false
    },
    {
      title: 'The Boys',
      subtitle: '2019 ‧ Drama ‧ 4 temporadas',
      image: 'https://lrmonline.com/wp-content/uploads/2019/07/the-boys-THBY_S1_PR_Screener_1280x720_rgb.jpg',
      description: '"The Boys" é uma série de televisão baseada nos quadrinhos de mesmo nome, criados por Garth Ennis e Darick Robertson. Ambientada em um mundo onde super-heróis são corporativos e corruptos, a série segue um grupo de vigilantes que busca expor a verdade por trás da fachada de bondade dos heróis. Com uma abordagem satírica e ultraviolenta, "The Boys" explora temas como poder, corrupção, e moralidade em um contexto contemporâneo. A série se destaca por seu humor ácido, personagens complexos e crítica social afiada, oferecendo uma visão distorcida e provocativa do gênero de super-heróis.',
      isExpanded: false,
      isFavorited: false
    },
    {
      title: 'Mr. Robot',
      subtitle: '2015 ‧ Thriller ‧ 4 temporadas',
      image: 'https://gizmodo.uol.com.br/wp-content/blogs.dir/8/files/2015/06/1283167174773716072.jpg',
      description: 'Mr. Robot" é uma série de televisão que segue Elliot Alderson, um jovem hacker com habilidades excepcionais, que se une a um grupo de ativistas para derrubar grandes corporações e desafiar o sistema financeiro corrupto. A série aborda temas profundos como alienação, identidade e poder, enquanto Elliot luta com sua própria saúde mental e conexões pessoais complexas. Com uma narrativa envolvente e estética visual única, "Mr. Robot" é aclamada por sua escrita inteligente, reviravoltas surpreendentes e performances marcantes, explorando o lado sombrio da tecnologia e o impacto da sociedade digital em nossas vidas.',
      isExpanded: false,
      isFavorited: false
    }
  
  ];

  constructor() {}

  toggleExpand(series: Series) {
    series.isExpanded = !series.isExpanded;
  }

  toggleFavorite(series: Series) {
    series.isFavorited = !series.isFavorited;
  }
}
