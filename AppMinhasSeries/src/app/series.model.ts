export interface Series {
    title: string;
    subtitle: string;
    image: string;
    description: string;
    isExpanded: boolean;
    isFavorited: boolean;
  }
  